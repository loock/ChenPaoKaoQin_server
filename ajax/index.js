const router = require('koa-router')();
router.prefix('/ajax')

router.get('/teaGetRunTimes',require('./teaGetRunTimes').fn);
router.post('/messageAdd',require('./messageAdd').fn);
router.get('/getschoolinfo',require('./getSchoolInfo').fn);
module.exports = router
