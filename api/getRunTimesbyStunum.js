const request = require('request');
/*******************stuGetInfo***********************
@get
@parames
  id char(9)
@callback

*/

exports.fn = async (ctx,next)=>{
    var params = ctx.query;
    if (
    params.stuNum == null
  ) {
    ctx.body = {
        err: "参数不完整"
      }
  } else {
    try{
      let sql = `call getRunTimesbyStunm("${params.stuNum}")`;
      let result = await ctx.mysql.query(sql);
      ctx.body = result[0];
    }catch(err){
      ctx.body = err.code;
    }
  }
}