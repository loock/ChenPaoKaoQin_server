const request = require('request');

/********************savePoints************************
@post
@params
  stuNum:string
  points:json
  step:int
  distance: double
@callback
  err: json //错误信息
  0    //正确执行
*/

exports.fn = async(ctx, next) => {
  var params = ctx.request.body;
  // console.log(params)
  if (
    params.stuNum == null ||
    params.points == null ||
    params.step == null ||
    params.distance == null
  ) {
    ctx.body = {
      err: "参数不完整"
    }
  } else {
    try {
      let points = JSON.stringify(params.points)
      let sql = `call pointAdd("${params.stuNum}",${points},${params.step},${params.distance})`;
      let result = await ctx.mysql.query(sql);
      ctx.body = result;
      console.log(params.stuNum + "===的跑步数据保存成功！");
    } catch (err) {
      console.log(JSON.stringify(err))
      ctx.body = {
        err: err.code
      };
    }
  }
}