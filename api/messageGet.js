const request = require('request');

/**************maessageGet******************
@get
@params
  receiveGroup:smallint(10)
  skip:  int(10) unsigned
  limit:  int(10) unsigned
@callback
  err:
  fields:
    id
    title
    body
    feedback
    owner
    priority
    createdAt
    endTime
    hits
*/

exports.fn = async(ctx, next) => {
  var params = ctx.query;
  if (params.id != null){
    try {
      let sql = `call messageGet_byID(${params.id})`;
      let result = await ctx.mysql.query(sql);
      ctx.body = result[0][0];
    } catch (err) {
      ctx.body = {
        err: err.code
      };
    }
    return;
  }
  if (
    params.skip == null ||
    params.limit == null ||
    params.receiveGroup == null
  ) {
    ctx.body = {
      err: "参数不完整"
    }
  } else {
    try {
      let sql = `call messageGet(${params.receiveGroup},${params.skip},${params.limit})`;
      let result = await ctx.mysql.query(sql);
      ctx.body = result[0];
    } catch (err) {
      ctx.body = {
        err: err.code
      };
    }
  }
}