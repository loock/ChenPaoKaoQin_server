const request = require('request');

/*****************stuUpdate****************
@post
@params
  stuNum
    char(9)
  password
    varchar(20)
  name
    varchar(20)
  phone
    char(20)
  sex
    char(1)
  campusID
    smallint（10）
  collegeID
    smallint（10）
  majorID
    smallint（10）
  classID
    smallint（10）
  weChart
    varchar(50)
  wOpenID
    varchar(50)
@callback


*/

exports.fn = async (ctx,next)=>{
    var params = ctx.request.body;
    if (
    params.stuNum == null ||
    params.password == null ||
    params.name == null ||
    params.phone == null ||
    params.sex == null ||
    params.campusID == null ||
    params.majorID == null ||
    params.classID == null ||
    params.weChart == null ||
    params.wOpenID == null
  ) {
    ctx.body = {
        err: "参数不完整"
      }
  } else {
    try{
      let sql = `call stuAdd("${params.stuNum}","${params.password}","${params.name}","${params.phone}",${params.sex},${params.campusID},${params.collegeID},${params.majorID},${params.classID},"${params.weChart}","${params.wOpenID}")`;
      let result = await ctx.mysql.query(sql);
      ctx.body = result;
    }catch(err){
      ctx.body = err.code;
    }
  }
}