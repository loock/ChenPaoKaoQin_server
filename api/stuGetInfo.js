const request = require('request');
/*******************stuGetInfo***********************
@get
@parames
  stuNum char(9)
@callback
  stuNum
  name
  phone
  sex
  campus
  college
  major
  class
  weChart
  wOpenID
*/

exports.fn = async (ctx,next)=>{
    var params = ctx.query;
    if (
    params.stuNum == null
  ) {
    ctx.body = {
        err: "参数不完整"
      }
  } else {
    try{
      let sql = `call stuGetInfo("${params.stuNum}")`;
      let result = await ctx.mysql.query(sql);
      ctx.body = result[0];
    }catch(err){
      ctx.body = err.code;
    }
  }
}