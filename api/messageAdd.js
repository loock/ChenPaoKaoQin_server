const request = require('request');

/*******************messageAdd**********************
@post
@params
  title
    varchar(20)
  body
    varchar(20)
  receivGroup
    smallint(10)
  ownerID
    char(9)
  priority
    enum
@callback
  err： json //错误信息
  0 //正确执行

*/

exports.fn = async (ctx,next)=>{
    var params = ctx.request.body;
    if (
    params.title==null || 
    params.body==null || 
    params.receivGroup==null || 
    params.ownerID==null || 
    params.priority==null
  ) {
    ctx.body = {
        err: "参数不完整"
      }
  } else {
    try{
      let sql = `call messageAdd("${params.title}","${params.body}",${params.receivGroup},"${params.ownerID}",${params.priority})`;
      let result = await ctx.mysql.query(sql);
      ctx.body = result;
    }catch(err){
      ctx.body = err.code;
    }
  }
}