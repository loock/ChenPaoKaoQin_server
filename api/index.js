const router = require('koa-router')();
router.prefix('/api')

router.post('/pointAdd',require('./pointAdd').fn);
router.get('/messageGet',require('./messageGet').fn);
router.post('/messageAdd',require('./messageAdd').fn);
router.get('/stuGetRunTimes',require('./stuGetRunTimes').fn);
router.get('/getRunTimesbyStunum',require('./getRunTimesbyStunum').fn);
router.get('/getMap',require('./getMap').fn);
router.get('/stuGetInfo',require('./stuGetInfo').fn);
router.post('/stuUpdate',require('./stuUpdate').fn);
router.post('/teaAdd',require('./teaAdd').fn);
router.get('/teaGetInfo',require('./teaGetInfo').fn);
router.post('/teaUpdate',require('./teaUpdate').fn);
router.get('/stuLogout',require('./stuLogout').fn);

module.exports = router
