const request = require('request');
/*******************stuGetInfo***********************
@get
@parames
  stuNum char(9)
@callback

*/

exports.fn = async (ctx,next)=>{
    var params = ctx.query;
    try{
      let sql = `call stuGetRunTimes("${params.stuNum}")`;
      console.log(sql);
      let result = await ctx.mysql.query(sql);
      console.log(result);
      ctx.body = result[0][0];
    }catch(err){
      ctx.body = err.code;
    }
}