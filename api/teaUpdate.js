const request = require('request');

/*****************stuUpdate****************
@post
@params
  id
    char(9)
  password
    varchar(20)
  name
    varchar(20)
  tel
    char(15)
  phone
    char(15)
  sex
    char(1)
  email
    varchar(40)
  post
    enum
  collegeID
    smallint（10）
  classID
    smallint（10）
@callback


*/

exports.fn = async (ctx,next)=>{
    var params = ctx.request.body;
    if (
    params.id == null ||
    params.password == null ||
    params.name == null ||
    params.tel == null ||
    params.phone == null ||
    params.sex == null ||
    params.email == null ||
    params.post == null ||
    params.campusID == null ||
    params.classID == null
  ) {
    ctx.body = {
        err: "参数不完整"
      }
  } else {
    try{
       let sql = `call teaAdd("${params.id}","${params.password}","${params.name}","${params.tel}","${params.phone}",${params.sex},"${params.email}",${params.post},${params.campusID},${params.classID})`;
      let result = await ctx.mysql.query(sql);
      ctx.body = result;
    }catch(err){
      ctx.body = err.code;
    }
  }
}