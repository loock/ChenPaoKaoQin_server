const request = require('request');

/***************stuAdd*********************
@post
@params
  stuNum
    char(9)
  password
    varchar(20)
  name
    varchar(20)
  phone
    char(20)
  sex
    char(1)
  campusID
    smallint（10）
  collegeID
    smallint（10）
  majorID
    smallint（10）
  classID
    smallint（10）
@callback

*/
const router = require('koa-router')();
router.prefix('/stu')

router.post('/stuAdd',async(ctx) => {
  var params = ctx.request.body;
  if (
    params.stuNum == null ||
    params.password == null ||
    params.name == null ||
    params.phone == null ||
    params.sex == null ||
    params.campusID == null ||
    params.majorID == null ||
    params.superID == null ||
    params.classID == null
  ) {
    ctx.body = {
      err: "参数不完整"
    }
  } else {
    try {
      let sql = `call stuAdd("${params.stuNum}","${params.password}","${params.name}","${params.phone}",${params.sex},${params.campusID},${params.collegeID},"${params.majorID}",${params.classID},${params.superID})`;
      let result = await ctx.mysql.query(sql);
      ctx.body = result;
    } catch (err) {
      console.log(err)
      ctx.body = 
      {
        err : '001'
      }
    }
  }
});
module.exports = router