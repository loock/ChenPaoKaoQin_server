const router = require('koa-router')();


router.get('/', async (ctx, next) => {
  await ctx.render('index', {
    title: 'Hello Koa 2!'
  })
})

router.get('/userinfo', async (ctx, next) => {
  var params = ctx.query;
  let result = await ctx.mysql.query(`call teaGetInfo(${params.id})`);
  console.log(JSON.stringify(result[0][0]));
  await ctx.render('userinfo',{
    u: result[0][0]
  })
})

router.get('/tables', async (ctx, next) => {
  let result = await ctx.mysql.query('select * from text');
  await ctx.render('tables',{
    seq: result
  })
})

router.get('/messageadd', async (ctx, next) => {
  await ctx.render('messageAdd',{
    seq: 'result'
  })
})

router.get('/runTimesTable', async (ctx, next) => {
  var params = ctx.query;
  let result = await ctx.mysql.query(`call teaGetRunTimes(${params.class})`);
  console.log(JSON.stringify(result[0]));
  await ctx.render('runTimesTable',{
    seq: result[0]
  })
})
module.exports = router
