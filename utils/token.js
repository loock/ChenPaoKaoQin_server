const router = require('koa-router')();
router.prefix('/api')
router.all('/*',async(ctx, next) => {
  if ( ctx.request.method == "GET"){
    var params = ctx.query;
  }else if ( ctx.request.method == "POST"){
    var params = ctx.request.body;
  }else{
    ctx.body = {
        err: "3-无操作权限"
      };
    return
  }
  
  let sql = `call checkToken("${params.stuNum}","${params.token}","${ctx.request.ip}")`;
  console.log(sql);
  try {
    let result = await ctx.mysql.query(sql);
    let code = result[0][0].result;
    if (code == 7) {
      await next();
    } else {
      ctx.body = {
        err: code+"-无操作权限"
      };
    }
  }catch(err){
    console.log(err);
     ctx.body = {
        err: '404-token err'
      };
  }
})

module.exports = router