const noAuth=[
    /^\/manage\/?users\/(.*){1,}$/,
    /^\/manage\/?stylesheets\/(.*){1,}$/,
    /^\/manage\/?images\/(.*){1,}$/,
    /^\/manage\/?javascripts\/(.*){1,}$/,
    /^\/manage\/?swf\/(.*){1,}$/,
    /^\/manage\/?fonts\/(.*){1,}$/,
    /^\/{0,1}$/,
    /^\/index$/,
    /^\/app\/(.*){1,}$/,
    /^\/createOrder$/,
    /^\/favorite\.ico$/,
    /^\/MP_verify_OjFyJzQt0lRg1sZK\.txt$/,
    /^\/setUserCookie$/,
    /^\/confirm$/,
    /^\/overOrder$/,
    /^\/findOrder$/,
    /^\/seekdeath$/,
    /^\/userdata$/
];

let response_formatter=ctx=>{
    for(let item in noAuth)if(new RegExp(noAuth[item]).test(ctx.path))return true;
    return false;
}

module.exports = () => {
    return async (ctx, next) => {
        if(!response_formatter(ctx)){
            if(!ctx.session.id){
                let url='/users/login';
                if(ctx.path.split('/')[1]=='manage')url='/manage/users/login';
                else ctx.status=404;
                ctx.redirect(url);
            }else await next();
        }
        else await next();
    }
};